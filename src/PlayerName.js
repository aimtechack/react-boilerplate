import React, {Component} from 'react';

export default class PlayerName extends Component {
    constructor(props)
    {
        super(props);

        this.state = {
            name: ""
        };
    }

    handleSubmit(e)
    {
        e.preventDefault();

        let name = this.state.name.trim();

        if (name.length === 0)
        {
            return;
        }

        this.props.onPlayerNameSubmit(name);
    }

    handleNameChange(e)
    {
        this.setState({name: e.target.value});
    }

    onClick(value)
    {
        this.props.onPlayerNameSubmit(value);
    }

    render()
    {
        return (
            <div className="row">
                <div className="col-xs-12">
                    <a className="language-menu-reset" onClick={this.onClick.bind(this, 'reset')}>Změnit jazyk</a>
                    <form className="player-name-form" onSubmit={this.handleSubmit.bind(this)}>
                        <input type="text" className="player-name-input" placeholder="Zadejte jméno" value={this.state.name} onChange={this.handleNameChange.bind(this)} autoFocus="true" />
                        <br />
                        <input type="submit" value="Odeslat" className="btn-default player-name-submit" />
                    </form>
                </div>
            </div>
        );
    }
}