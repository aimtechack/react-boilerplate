import React, { Component } from 'react';

export default class Cw extends Component {


	static propTypes = {
		tahy: React.PropTypes.array.isRequired,
		posliZnak:React.PropTypes.func,
    cw: React.PropTypes.string
    }

	constructor(props) {
        super(props);
        this.state = {counter: 0};

        //this.state = {text: "text",cw:"DIKOBRAZ##S#F##\n#Z#Z#A#L#KRTEK#\nPOZNÁMKA#A#I#IR\n#L#Á#E#TAMTAM#\nJASMÍN#O#A#U#B#\n#C#I#O###R#LIER\nVESTA#BÍLÁ#E#R#\nI#P#S####D#KOLO\nO#A#FIT#P###DY#\nLINKA###O#A#Z##\nA#D#L#CHLÉB#N##\n#SESTRA#E#CLAVE\n##X###D###D#K##"}
        //this.state = {text: "text",cw:"ACHILLES##M#A##\nO#T#A#W#LEASH#\nPLEASURE#E#B#ON\nL#L#R#ANAGRAM#\nFAMINE#R#T#A#E#\n#G#A#L###H#HILL\nBEING#WAVE#A#A#\nE#N#O####R#MANY\nA#T#ASS#O###ND#\nCHEST###N#A#T##\nH#G#E#JACOB#L##\nLEXEME#E#CZECH\nR###W###D#R##\n"}
        
    }


	handleZobraz(e){
        this.setState(previousState => {
            return {text: "ahoj" };
        });
    }

	handleVstup(e) {
    	var vstup = e.target.value;
        console.log("Napsal:" +vstup);

        this.setState(previousState => {
            return {text: 	vstup };
        });
  	}

  	handleZnak (x, y, e) 
  	 {
  		console.log(e);
  		this.props.posliZnak(x, y, e.target.value);
  	}

  	renderCrossWord(cw) {
  		let list = [];
  		let lineNo = 0;
		let colNo = 0;

		let pole = [];

		this.props.tahy.forEach(function(tah) {
			if (!pole[tah.sloupec]) {
				pole[tah.sloupec] = [];
		}
			pole[tah.sloupec][tah.radek] = true;
		});	

  		cw.split('\n').forEach((line) => {
  			colNo=0;
			list.push(<tr key={lineNo++}>{				
		  		line.split("").map((ch) => {
		  			let key = (colNo++);
					if (ch === "#") {
						return (<td key={key}></td>);
					} else {

						let jeV = pole[colNo-1] ? pole[colNo-1][lineNo-1] : false;

						if(jeV){
							return (<td key={key}><input 

									style={{background: 'red'}}

							 type="text"  size="1" maxLength="1"/></td>);
						}else{
							return (<td key={key}><input type="text" onChange={this.handleZnak.bind(this, colNo-1, lineNo-1)} defaultValue={ch} size="1" maxLength="1"/></td>);
						}
						
					}
		  		})
			}</tr>);
  		});

  		return list;
  	}
 	


  render() {

    return (
    <div className="a"><table><tbody>{this.renderCrossWord(this.props.cw)}</tbody></table>
    	{this.props.cw}
    	{
    		this.props.tahy.map(function(player) {
			   return <div>{player.radek} : {player.sloupec} </div>
			   			   // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
			})
    	}
    </div>
    );
  }
}


