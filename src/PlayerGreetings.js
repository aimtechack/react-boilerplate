import React, {Component} from 'react';

export default class PlayerGreetings extends Component {
    static propTypes = {
        playerName: React.PropTypes.string.isRequired
    };

    render()
    {
        return (
            <div>
                <h1>Vítejte, {this.props.name}!</h1>
            </div>
        );
    }
}