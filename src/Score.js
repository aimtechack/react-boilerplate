import React, {Component} from 'react';

export default class Score extends Component {
    static propTypes = {
        score: React.PropTypes.array.isRequired
    };

    constructor(props)
    {
        super(props);
    }

    render()
    {
        var usersScore = this.props.score.map((value, index) => {
            return (
                <div className="col-xs-12">
                    <div className="col-xs-6">{index}</div>
                    <div className="col-xs-6">{value}</div>
                </div>
            );
        });

        return (
            <div className="users-score row">
                <h2>Skóre</h2>
                <div className="col-xs-12">
                    <div className="col-xs-6">
                        <h3>Uživatel</h3>
                    </div>
                    <div className="col-xs-6">
                        <h3>Skóre</h3>
                    </div>
                </div>
                {usersScore}
            </div>
        );
    }
}
