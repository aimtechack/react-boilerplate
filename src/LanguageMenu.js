import React, {Component} from 'react';
import LanguageMenuItem from './LanguageMenuItem';

export default class LanguageMenu extends Component {
    static languageCombinations = {
        'de': [
            'en', 'cs',
        ]
        ,
        'es': [
            'cs', 'en'
        ]
        ,
        'cs': [
            'en'
        ]
        ,
        'fr': [
            'en'
        ]
    };

    static propTypes = {
        onLanguageChoose: React.PropTypes.func.isRequired,
        showLanguagesCross: React.PropTypes.bool.isRequired
    };

    static contextTypes = {
        languageCross: React.PropTypes.string.isRequired,
        languageTips: React.PropTypes.string.isRequired
    };

    constructor(props)
    {
        super(props);

        this.state = {};
    }

    onClick(value)
    {
        this.props.onLanguageChoose(value);
    }

    render()
    {
        if (this.props.showLanguagesCross === false)
        {
            var languageItems = Object.keys(LanguageMenu.languageCombinations).map((value, index) => {
                return (
                    <div className="col-xs-12 col-sm-5 col-md-3" onClick={this.onClick.bind(this, value)}>
                        <LanguageMenuItem language={value} key={value} />
                    </div>
                );
            });
        }
        else
        {
            let tipsLanguage = this.context.languageTips;

            var languageItems = LanguageMenu.languageCombinations[tipsLanguage].map((value, index) => {
                return (
                    <div className="col-xs-12 col-sm-5 col-md-3" onClick={this.onClick.bind(this, value)}>
                        <LanguageMenuItem language={value} key={value} />
                    </div>
                );
            })
        }

        let languageType = this.props.showLanguagesCross === true ? "křížovku" : "nápovědu";

        return (
            <div>
                {this.props.showLanguagesCross === true ? <a className="language-menu-reset" onClick={this.onClick.bind(this, 'reset')}>Změnit jazyk</a> : null}
                <h1 className="language-header">Zvolte prosím jazyk pro {languageType}</h1>
                {languageItems}
            </div>
        );
    }
}
