import React, { Component } from 'react';

export default class CrossWordsTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {pole:[], text: "text"};

    }

    static propTypes = {
        words: React.PropTypes.array.isRequired,
        pocetRadek: React.PropTypes.number.isRequired,
        pocetSloupcu: React.PropTypes.number.isRequired,
        tahy: React.PropTypes.array.isRequired,
    }


    renderCrossWord(words) {
        var style = {
            background: 'black',
            borderWidth: "thin",
            width: '35px',
            height: '30px',
            borderColor: 'black',
        };

        var styleWord = {
            background: 'blue',
            color: 'white',
            borderColor: 'black',
            borderWidth: "thin",
            width: '35px',
            height: '30px',
            textAlign: 'center'
        }

        var styleSelected = {
            background: 'red',
            color: 'white',
            borderColor: 'black',
            borderWidth: "thin",
            width: '35px',
            height: '30px',
            textAlign: 'center'
        }

        let list = [];
        let lineNo = 0;
        let colNo = 0;



        let pole = [];

        this.props.tahy.forEach(function(tah) {
            if (!pole[tah.sloupec]) {
                pole[tah.sloupec] = [];
            }
            pole[tah.sloupec][tah.radek] = true;
        });


        let i = 0;
        //sconsole.log("words:" + words);
        //console.log(words);
        Object.keys(words).map((index)=> {
            i++;
           // console.log("klic:" + index);
            var obj = words[index];
            if((index % this.props.pocetSloupcu ) == 0) {
               // console.log("vkladam:" + index);
                list.push(<div key={i+1250} className="crossword_cell">{
                    this.neco(list,style,styleWord,words,index,styleSelected,pole)
                }</div>);
            }
        });

        return list;
    }

    neco(list,style,styleWord,words,index,styleSelected,pole){
        let index2 = parseInt(index);
       // console.log("index:" + index);
       // console.log(this.props.words);
        for(var j = 0;j<this.props.pocetSloupcu;j++){
           // console.log("index:" + index2 + " j:" + j);
            var cislo  = parseInt(index2 + j);
           // console.log("cislo:" + cislo + " j:" + j);


            var x = (cislo % this.props.pocetSloupcu);
            var y = Math.floor(cislo / this.props.pocetSloupcu);
            //console.log("souradnice x:" + x + " y:" + y);


            let jeV = pole[x] ? pole[x][y] : false;
           // console.log("je VVVVVV:" + jeV);

            let obj = this.props.words[cislo];
            if(obj == null) {
                //console.log("objekt je null:" + index2);
                list.push(<input key={j+index2} style={style} type="text" maxLength="1" size="1"/>);
            } else {
                //console.log("objekt NENI null:" + index2);
                let style = styleWord;
                if(jeV){
                    style = styleSelected;
                }

                if(obj.first){
                    list.push(<input key={obj.key} style={style} placeholder={obj.key} onChange={this.handleVstup(obj.key)} type="text" maxLength="1" size="1"/> );
                }else {
                    list.push(<input key={obj.key} style={style} onChange={this.handleVstup(obj.key)} type="text" maxLength="1" size="1"/> );
                }

            }
        }
    }

    handleVstup(i) {
        return function (e) {
            var key = i;
            var vstup = e.target.value;
            var ob = this.props.words[key]
            //console.log("Kliknuto na:"+ob  + " hodnota:" + ob.value + " klic:" + key);
            this.props.onTextChange(ob,vstup);
        }.bind(this) //important to bind function
    }

    render() {
        //console.log("Renderuji template");
        var pole = [];
        return (
            <div id="crossword" className="col-md-6 col-md-offset-3 centered">
                    {this.renderCrossWord(this.props.words)}
            </div>
        );
    }

}
