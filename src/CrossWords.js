import React, { Component } from 'react';
import CrossWordsTemplate from './CrossWordsTemplate'


export default class CrossWords extends Component {

    constructor(props) {
        super(props);
        this.state = {pole:[],counter: 0 ,pocetRadek: 0,pocetSloupcu: 0,text:"text",data:"ahoj"};

    }

    static propTypes = {
        tahy: React.PropTypes.array.isRequired,
        posliZnak:React.PropTypes.func,
        cw: React.PropTypes.string.isRequired,
    }

    componentDidMount(){
        this.parseData(this.props.cw);
    }

    componentWillReceiveProps(nextProps) {
       // console.log("props cw:" + nextProps.cw);
        this.parseData(nextProps.cw);
    }

    parseData(data){
        let list = [];
        let lineNo = 0;
        let colNo = 0;
        let pocetSloupcu = 0;


        var pole = [];
        var first = true;
        data.split('\n').forEach((line) => {
            list.push(<div key={lineNo++} className="crossword_cell">{
                line.split("").map((ch) => {
                    let key = (colNo++);

                    if(lineNo == 1){
                        pocetSloupcu++;
                    }
                    if (ch === "#") {
                        pole[key] = null;
                        first = true;
                    } else {
                        var data = {key: key, value: ch, first:first};
                        if(first){
                          //  console.log(" key:" + key + " value:" + ch + " first:" +first);
                        }
                        first = false;
                        pole[key] = data;
                    }
                })
            }</div>);
        });

        //console.log("pocet radek:" + lineNo);
        //console.log("pocet sloupcu:" + colNo);
        //console.log("pocet sloupcu opravdu:" + pocetSloupcu);
        //console.log(pole);
        this.setState({pole: pole, pocetRadek: lineNo,pocetSloupcu:pocetSloupcu,data:data});

    }


    onTextChange(vstup,vyplneno){
       // console.log("Vystup cross:" + vstup.value);
       // console.log("Vyplneno:" + vyplneno);
        var pocetSloupcu = parseInt(this.state.pocetSloupcu);
        var pom = parseInt(vstup.key);


        var x = (pom % pocetSloupcu);
        var y = Math.floor(pom / pocetSloupcu);
        //console.log("x:" + x + " y:" + y);
        this.props.posliZnak(x, y,vyplneno);

    }

    handleZnak (x, y, vyplneno)
    {
        console.log(e);

    }

    render() {

       // console.log("loguju");
       // console.log(this.state.pocetRadek);
        return (
            <div className="home-page">

                <CrossWordsTemplate onTextChange={this.onTextChange.bind(this)} words={this.state.pole} pocetRadek={this.state.pocetRadek} pocetSloupcu={this.state.pocetSloupcu} tahy={this.props.tahy} />

            </div>


        );
    }

}



