import React, {Component} from 'react';

export default class LanguageMenuItem extends Component {
    static propTypes = {
        language: React.PropTypes.string.isRequired
    };

    static contextTypes = {
        languageCross: React.PropTypes.string.isRequired,
        languageTips: React.PropTypes.string.isRequired
    };

    constructor(props)
    {
        super(props);

        this.state = {};
    }

    render()
    {
        let classNameVar = 'language ' + this.props.language;

        return (
            <div className={classNameVar}></div>
        );
    }
}
