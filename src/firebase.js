import React, { Component } from 'react';
import { firebaseRef } from './connection';
import Immutable from 'immutable';
import Cw from './cw';
import CrossWords from './CrossWords';
import PlayerGreetings from './PlayerGreetings';
import Score from './Score';
import Hints from './hints';


export default class Firebase extends Component {
	
	static propTypes = {
		languageCross: React.PropTypes.string.isRequired,
		languageTips: React.PropTypes.string.isRequired,
		playerName: React.PropTypes.string.isRequired
	}

	constructor(){
        super();
        this.state = {counter: 0};
        this.state = {
        	text: "text",
        	cw: "",

        	tahy: [],
			skore: []
        };


    }

   /* componentDidMount(){
    	//console.log("Jazyk napovedy: "+this.props.languageTips);
    	//console.log("Jazyk krizovky: "+this.props.languageCross);

    	let self = this;
    	firebaseRef.child("/tah/adad/").on("child_added", function(snapshot) {

			var newState = Immutable.List(self.state.tahy);
			newState = newState.push(snapshot.val());
		  	self.setState({tahy: newState.toArray()});		  	
		});
    	let uri = "krizovky/"+self.props.languageTips+"/"+self.props.languageCross+"/prvni";
    	//console.log(uri);
		firebaseRef.child("krizovky/"+self.props.languageTips+"/"+self.props.languageCross+"/prvni").on("value", function(snapshot) {
		  //console.log("Vypisuji krizovku :" + snapshot.val());
		  let krizovka = snapshot.val();
		  self.setState({cw:krizovka});

		}, function (errorObject) {
		  console.log("The read failed: " + errorObject.code);
		});
    }*/

	componentDidMount(){
		let self = this;
		firebaseRef.child("/tah/adad/").on("child_added", function(snapshot) {

			var newState = Immutable.List(self.state.tahy);
			if(snapshot.val().hrac!==self.props.playerName){

			newState = newState.push(snapshot.val());
			}

			self.setState({tahy: newState.toArray()});
		});

        firebaseRef.child("/hry/adad/skore/").on("value", (snapshot) => {
            let snapshotVal = snapshot.val();

            let actualScore = this.state.skore;
            actualScore[snapshotVal.hrac] = snapshotVal.hodnota;

            self.setState({skore: actualScore});
		});

		firebaseRef.child("krizovky/"+self.props.languageTips+"/"+self.props.languageCross+"/prvni").on("value", function(snapshot) {
			//console.log("Vypisuji krizovku :" + snapshot.val());
			let krizovka = snapshot.val();
			self.setState({cw:krizovka});



		}, function (errorObject) {
			console.log("The read failed: " + errorObject.code);


		});

	}

	handleZobraz(e){
        this.setState(previousState => {
            return {text: "ahoj" };
        });
    }

	handleVstup(e) {
	//	 var myFirebaseRef = new Firebase("https://popping-heat-2447.firebaseio.com/tah/adad");
		

	}




		
  	posliZnak(x,y,z){
  		let hrac = this.props.playerName;
  		
  		firebaseRef.child("/tah/adad").push({
							"hrac":hrac,
							"sloupec":x,
							"radek":y,
							"pismeno":z
			});
	}


  render() {

	  //console.log("props cw render:" + this.state.cw);
    return (
    <div className="a">

                    <PlayerGreetings name={this.props.playerName} />

                	<CrossWords cw={this.state.cw} tahy={this.state.tahy} posliZnak={this.posliZnak.bind(this)} />
                    <Score score={this.state.skore} />
                    
                   <Hints hints="0,0&espin&9,1&Topo&0,2&Nota&13,2&iridium&7,3&tomtom&0,4&jazmin&11,5&lier&0,6&chaleco&6,6&blanco&11,7&rueda&4,8&ajuste&12,8&dysprosium&0,9&linea&6,10&pan&1,11&hermana&10,11&clave#0,6&viola&1,0&aislamiento&2,6&spandex&3,0&anunciar&4,6&asfalto&5,0&hombro&6,10&Canada&7,0&oro&8,8&campo&9,1&companero&10,0&strontium&11,1&titular&12,0&ferrum&12,7&distintivo&13,1&kimberly" colNo={15} />
                    
                   
    </div>
    );
  }
}
