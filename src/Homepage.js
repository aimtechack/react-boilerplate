import React, {Component} from 'react';
import LanguageMenu from './LanguageMenu';
import Firebase from './firebase';
import PlayerName from './PlayerName';

export default class Homepage extends Component {
    static childContextTypes = {
        languageCross: React.PropTypes.string,
        languageTips: React.PropTypes.string
    };

    static defaultSetValues = {
        showLanguagesTips: true,
        showLanguagesCross: false,
        languageTips: 'cs',
        languageCross: 'en',
        playerName: ''
    };

    constructor(props)
    {
        super(props);

        this.state = Homepage.defaultSetValues;
    }

    getChildContext()
    {
        return {
            languageCross: this.state.languageCross,
            languageTips: this.state.languageTips
        };
    }

    onLanguageMenuClick(value)
    {
        if (value === 'reset')
        {
            this.setState(Homepage.defaultSetValues);
        }
        else
        {
            let showLanguagesCrossVar = (this.state.showLanguagesTips === true && this.state.showLanguagesCross === false) ? true : false;
            let languageTipsVar = showLanguagesCrossVar === true ? value : this.state.languageTips;
            let languageCrossVar = showLanguagesCrossVar === true ? this.state.languageCross : value;

            this.setState({
                showLanguagesTips: false,
                showLanguagesCross: showLanguagesCrossVar,
                languageTips: languageTipsVar,
                languageCross: languageCrossVar
            });
        }

    }

    onPlayerNameSubmit(name)
    {
        if (name === 'reset')
        {
            this.setState(Homepage.defaultSetValues);
        }
        else
        {
            this.setState({playerName: name});
        }
    }

    render()
    {
        let showLanguages = (this.state.showLanguagesTips || this.state.showLanguagesCross);

        return (
            <div>
                {showLanguages ? <LanguageMenu showLanguagesCross={this.state.showLanguagesCross} onLanguageChoose={this.onLanguageMenuClick.bind(this)} />
                    : (this.state.playerName.length === 0 ? <PlayerName onPlayerNameSubmit={this.onPlayerNameSubmit.bind(this)} />
                    : <Firebase playerName={this.state.playerName} languageTips={this.state.languageTips} languageCross={this.state.languageCross} />)}
            </div>
        );
    }
}
