import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Cw from './cw';
import Firebase from './firebase';
import Homepage from './Homepage';

ReactDOM.render(<App />, document.getElementById('root'));
