import React, { Component } from 'react';

export default class Hints extends Component {
	static propTypes = {
		hints: React.PropTypes.array.isRequired,
		colNo: React.PropTypes.number,
		
    }

    
    render()
    {
    	
        return (

            <div className="hints">{
            	this.props.hints.split("#").map((ch) => {
            		let i = 0;
            		var dvojice = "<div>";
            		let list = [];
       
					ch.split("&").map( 
		  				(hint) => {
		  					i++;
		  					if(i%2===1){
		  						var nasobek=1;
		  						var col=0;
		  						var row=0;
		  						var set=0;

		  						hint.split(",").map(
		  							(pulka)=>{
		  								if(set){
		  									row=pulka;
		  								}else{
		  									col=pulka;
		  								}
		  								set=1;
		  							})
		  						var pocet=15;
		  						var soucet=parseInt(row) * parseInt(pocet) + parseInt(col);
		  						dvojice +=soucet + "";

		  						 list.push(<div className="col-xs-1 ">{soucet}{
				               
				                }</div>);
		  						return soucet;
		  					}else{
		  						list.push(<div className="col-xs-3">{hint}{
				               
				                }</div>);
		  						return hint;
		  					}
		  					
		  				}


		  			)
		  			return list;
		  			//return <div>{dvojice}</div>;

		  		})
            }</div>
        );
    }
}
